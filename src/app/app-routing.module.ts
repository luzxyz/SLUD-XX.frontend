import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Import other modules

import { ScheduleComponent } from './schedule/schedule.component';
import { AppComponent } from './app.component';

// https://angular.io/api/router/Route
const routes: Routes = [
  { path: 'schedule', component: ScheduleComponent },
  { path: '**', component: AppComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
